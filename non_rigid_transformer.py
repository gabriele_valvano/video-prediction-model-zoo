import tensorflow as tf
from layers import stn_spline


class Transformer(object):

    def __init__(self, out_channels, is_training, name='Transformer'):
        """
        TODO documentation
        """
        self.out_channels = out_channels
        self.is_training = is_training
        self.name = name
        self.theta_values = None
        self.transformed_input = None
        self.future_frames = None
        self.hard_pred_frames = None

    def build(self, input_tensor, input_times, delta_times, input_to_transform=None, attention_map=None, reuse=tf.AUTO_REUSE):
        """
        Build the model.
        """
        with tf.variable_scope(self.name, reuse=reuse):
            shape = input_tensor.get_shape().as_list()
            dims = [shape[1], shape[2]]

            if input_to_transform is None:
                input_to_transform = input_tensor

            if attention_map is None:
                input_with_attention = input_tensor
            else:
                a_shape = attention_map.get_shape().as_list()
                a_dims = [a_shape[1], a_shape[2]]
                assert a_dims == dims
                if len(a_shape) == len(shape) - 1:
                    attention_map = tf.expand_dims(attention_map, -1)
                input_with_attention = tf.multiply(input_tensor, attention_map)

            # build a grid of 5x5 (25 points in total) and interpolate among the points
            self.theta_values = stn_spline.regress_theta(input_with_attention, input_times, delta_times, n_points=25)
            self.transformed_input = stn_spline.ThinPlateSpline2D(
                dims, cp_dims=[5, 5], num_channels=self.out_channels)([input_to_transform, self.theta_values])

            self.future_frames = self.transformed_input

        return self

    def get_theta_values(self):
        return self.theta_values

    def get_future_frames(self):
        return self.future_frames
