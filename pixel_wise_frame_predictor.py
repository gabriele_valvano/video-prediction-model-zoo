import tensorflow as tf
from architectures.unet import UNet
from architectures.mlp_tr_out import MLPTransformer
import numpy as np
from architectures.layers.rounding_layer import rounding_layer
from tensorflow import layers

# He initializer for the layers with ReLU activation function:
he_init = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False)
b_init = tf.zeros_initializer()


class FramePredictor(object):

    def __init__(self, n_composing_masks, is_training, n_filters=16, name='FramePredictor'):
        """
        Class for FramePredictor architecture.
        A UNet-like architecture is conditioned on the temporal information added in its bottleneck. The model outputs a
        pool of possible predictions obtained by are transforming the input using either a spatial transformer network.
        Such preliminary predictions are used together to predict actual future frame. Such frame is obtained by summing
        up the weighted sum of the prediction. The weighting maps are extracted by the decoder of the UNet for each
        channel of the input data independently.

        - - - - - - - - - - - - - - - -
        Notice that:
          - The network predicts the transformation parameters of an affine transformation to apply to the input image.
            It is good practice to initialize such parameters with the parameters of an identity transformation.
            In this particular case, the parameters are extracted by an MLP that takes as input both the features in the
            bottleneck and temporal information features: one can penalize the distance of the transformation matrix
            from an identity matrix, obtainable using the class method 'theta_regularization_loss(...)'.
        - - - - - - - - - - - - - - - -

        Example of usage:

             # build a frame predictor:
             frame_predictor = FramePredictor(n_composing_masks=8, is_training=True, n_filters=16,
                                             name='STNFramePredictor').build(tframe_hard_anatomy_t1,
                                                                             tframe_input_times,
                                                                             tframe_delta_times)
             # get predicted frame:
             tframe_hard_anatomy_t2_pred = frame_predictor.get_hard_pred_frames(one_hot=False)

             # add regularization loss on the transformer parameters:
             theta_values = frame_predictor.get_theta_values()
             theta_loss = frame_predictor.theta_regularization_loss(theta_values, batch_size=b_size)

        """
        self.n_composing_masks = n_composing_masks
        self.is_training = is_training
        self.nf = n_filters
        self.name = name
        self.theta_values = None

    def build(self, input_frames, input_times, delta_times, reuse=tf.AUTO_REUSE):
        """
        Build the model.
        mode: (str) can be 'spatial_transformer' or 'conv'
        """

        with tf.variable_scope(self.name, reuse=reuse):
            input_channels = input_frames.get_shape().as_list()[-1]

            # - - - - - - -
            # build only UNet encoder for now: (n_out=n_w_masks+1 becouse the last one is for the identity matrix)
            unet = UNet(incoming=input_frames, n_out=self.n_composing_masks * input_channels,
                        n_filters=16, is_training=self.is_training)
            encoder = unet.build_encoder()

            # get latent code:
            latent_code = encoder[-2]  # (this is the output layer of the encoder)

            shape = latent_code.get_shape().as_list()
            n_units = shape[1] * shape[2]
            ratio = 4  # 4 means that we will have 4 channels of anatomical + 1 channel of temporal information

            flat_code = layers.flatten(latent_code)
            flat_code = layers.dense(flat_code, units=n_units * ratio)
            flat_code = tf.nn.relu(flat_code)

            times = tf.concat((tf.expand_dims(input_times, 1),
                               tf.expand_dims(delta_times, 1)), axis=1)

            time_code = layers.dense(times, n_units)
            time_code = tf.layers.batch_normalization(time_code)
            time_code = tf.nn.relu(time_code)

            latent_code_with_time = tf.concat([flat_code, time_code], axis=-1)
            latent_code_with_time = tf.reshape(latent_code_with_time, [-1, shape[1], shape[2], ratio + 1])

            # - - - - - - -
            # compute set of transformed input frames (output of MLP #2) :

            # extract the parameters and apply the transformation matrix to the input frames,
            # using the transformer module:
            self.intermediate_transformations, self.theta_values = MLPTransformer(mlp_in=latent_code_with_time,
                                                                                  transformer_in=input_frames,
                                                                                  mlp_units_in=128,
                                                                                  mlp_units_hidden=128,
                                                                                  mlp_units_out=6 * self.n_composing_masks,
                                                                                  is_training=self.is_training,
                                                                                  k_prob=0.8,
                                                                                  name='MLP_out').build()

            # - - - - - - -
            # build the rest of the UNet
            _encoder = [el for el in encoder]
            _encoder[-2] = latent_code_with_time
            code = unet.build_bottleneck(_encoder)
            decoder = unet.build_decoder(code)
            output = unet.build_output(decoder)

            shape = input_frames.get_shape().as_list()
            shape[0] = -1
            shape.append(self.n_composing_masks)
            self.composing_masks_stack = tf.reshape(output, shape)

            # - - - - - - -
            # define predicted future frame:
            self.future_frames = tf.reduce_sum(self.intermediate_transformations * self.composing_masks_stack, axis=-1)

        return self

    def theta_regularization_loss(self, theta_values, batch_size):
        """
        Regularization loss for theta values: ideally should not be too far away from identity matrix (no big changes
        in time).
        :param theta_values: parameters of the transformation matrix to the input frames, computed in build()
        :param batch_size: (int) batch size (i.e. number of input frames)
        :return: regularization loss
        """
        with tf.variable_scope('ParameterEstimate_loss'):
            # parameters for identity matrices
            identity_params = np.array([[1., 0., 0.], [0., 1., 0.]], dtype=np.float32).flatten()
            identity_row = tf.tile(identity_params, [batch_size * self.n_composing_masks])
            identity = tf.reshape(identity_row, [batch_size, self.n_composing_masks * 6])

            params_loss = tf.reduce_mean(tf.squared_difference(theta_values, identity))

        return params_loss

    def get_composing_masks(self):
        return self.composing_masks_stack

    def get_future_frames(self):
        return self.future_frames

    def get_intermediate_transformations(self):
        return self.intermediate_transformations

    def get_theta_values(self):
        return self.theta_values
