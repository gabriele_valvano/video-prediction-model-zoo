import tensorflow as tf
from tensorflow import layers
import numpy as np
from tensorflow.contrib.image import interpolate_spline
bilinear_interpolation = tf.contrib.resampler.resampler


class ThinPlateSpline2D(tf.keras.layers.Layer):

    def __init__(self, input_volume_shape, cp_dims, num_channels, inverse=False, order=2, **kwargs):

        self.vol_shape = input_volume_shape
        self.data_dimensionality = len(input_volume_shape)
        self.cp_dims = cp_dims
        self.num_channels = num_channels
        self.initial_cp_grid = None
        self.flt_grid = None
        self.inverse = inverse
        self.order = order
        super(ThinPlateSpline2D, self).__init__(**kwargs)

    def build(self, input_shape):

        self.initial_cp_grid = nDgrid(self.cp_dims)
        self.flt_grid = nDgrid(self.vol_shape)

        super(ThinPlateSpline2D, self).build(input_shape)

    # @tf.contrib.eager.defun
    def interpolate_spline_batch(self, cp_offsets_single_batch):

        warped_cp_grid = self.initial_cp_grid + cp_offsets_single_batch

        if self.inverse:
            interpolated_sample_locations = interpolate_spline(train_points=warped_cp_grid,
                                                               train_values=self.initial_cp_grid,
                                                               query_points=self.flt_grid,
                                                               order=self.order)
        else:
            interpolated_sample_locations = interpolate_spline(train_points=self.initial_cp_grid,
                                                               train_values=warped_cp_grid,
                                                               query_points=self.flt_grid,
                                                               order=self.order)

        return interpolated_sample_locations

    def call(self, args, **kwargs):

        vol, cp_offsets = args

        # rescale offsets to the volume's size:
        # cp_offsets *= 40.

        interpolated_sample_locations = tf.map_fn(self.interpolate_spline_batch, cp_offsets)[:, 0]

        interpolated_sample_locations = tf.reverse(interpolated_sample_locations, axis=[-1])

        interpolated_sample_locations = tf.multiply(interpolated_sample_locations,
                                                    [self.vol_shape[1] - 1, self.vol_shape[0] - 1])
        warped_volume = bilinear_interpolation(vol, interpolated_sample_locations)
        warped_volume = tf.reshape(warped_volume, (-1,) + tuple(self.vol_shape) + (self.num_channels,))
        return warped_volume


def nDgrid(dims, normalise=True, center=False, dtype='float32'):
    """
    returns the co-ordinates for an n-dimensional grid as a (num-points, n) shaped array
    e.g. dims=[3,3] would return:
      [[0,0],[0,1],[0,2],[1,0],[1,1],[1,2],[2,0],[2,1],[2,2]]
    if normalized == False, or:
      [[0,0],[0,0.5],[0,1.],[0.5,0],[0.5,0.5],[0.5,1.],[1.,0],[1.,0.5],[1.,1.]]
    if normalized == True.
    """
    if len(dims) == 2:
        grid = np.expand_dims(np.mgrid[:dims[0], :dims[1]].reshape((2, -1)).T, 0)
    elif len(dims) == 3:
        grid = np.expand_dims(np.mgrid[:dims[0], :dims[1], :dims[2]].reshape((3, -1)).T, 0)
    else:
        # non supported input dimension: input must be 2D image or 3D volume
        raise Exception

    if normalise:
        grid = grid / (1. * (np.array([[dims]]) - 1))

        if center:
            grid = (grid - 1) * 2

    return tf.cast(grid, dtype=dtype)


def regress_theta(input_tensor, input_times, delta_times, n_points):

    # -------------
    # features extractor for input anatomy:
    conv1 = layers.conv2d(input_tensor, filters=20, kernel_size=5, strides=1, padding='same')
    conv1 = tf.nn.leaky_relu(conv1)
    conv1 = tf.layers.max_pooling2d(conv1, pool_size=2, strides=2, padding='same')

    conv2 = layers.conv2d(conv1, filters=20, kernel_size=5, strides=1, padding='same')
    conv2 = tf.nn.leaky_relu(conv2)
    conv2 = tf.layers.max_pooling2d(conv2, pool_size=2, strides=2, padding='same')

    conv3 = layers.conv2d(conv2, filters=20, kernel_size=5, strides=1, padding='same')
    conv3 = tf.nn.leaky_relu(conv3)
    conv3 = tf.layers.max_pooling2d(conv3, pool_size=2, strides=2, padding='same')

    flat_code = layers.flatten(conv3)
    flat_code = layers.dense(flat_code, units=100)
    flat_code = tf.nn.tanh(flat_code)

    # -------------
    # map for temporal information to the above feature space
    times = tf.concat((tf.expand_dims(input_times, 1),
                       tf.expand_dims(delta_times, 1)), axis=-1)

    time_code = layers.dense(times, 10)
    time_code = tf.nn.tanh(time_code)

    # -------------
    # Extract the points
    # these are 2D points: hence the output are n_points pairs (x, y) --> units=2*n_points

    latent_code_with_time = tf.concat([flat_code, time_code], axis=-1)
    theta = layers.dense(latent_code_with_time, units=2*n_points, kernel_initializer='zeros', bias_initializer='zeros')
    theta = tf.reshape(theta, (-1, n_points, 2))

    return theta
