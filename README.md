# Video Prediction Model Zoo

**Tensorflow implementation of several video prediction models.**

Please, note that this project is under construction, hence there could be bugs, 
incomplete files, etc. If you spot any of these, please let me know at the following 
email addresses:

  *gabriele.valvano@imtlucca.it* 

Enjoy the code! :)

---------------------
**How to use:**

[Under construction]

---------------------
**References:**

[Under construction]

---------------------

For problems, bugs, etc. please contact me at the following email addresses:

  *gabriele.valvano@imtlucca.it* 

Enjoy the code! :)


**Gabriele**