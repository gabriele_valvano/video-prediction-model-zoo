import tensorflow as tf
from utils.small_unet import SmallUnet
from utils.mlp_tr_out import MLPTransformer
from utils.mlp_in import MLP
from six.moves import reduce
import numpy as np

# He initializer for the layers with ReLU activation function:
he_init = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False)
b_init = tf.zeros_initializer()


class FramePredictor(object):

    def __init__(self, is_training, n_filters=16, name='FramePredictor'):
        """
        Class for FramePredictor architecture.
        #TODO FIX DOCUMENTATION

        - - - - - - - - - - - - - - - -
        Notice that:
          - The network predicts the transformation parameters of an affine transformation to apply to the input image.
            It is good practice to initialize such parameters with the parameters of an identity transformation.
            In this particular case, the parameters are extracted by an MLP that takes as input both the features in the
            bottleneck and temporal information features: one can penalize the distance of the transformation matrix
            from an identity matrix, obtainable using the class method 'theta_regularization_loss(...)'.
        - - - - - - - - - - - - - - - -

        Example of usage:

             # build a frame predictor in 'spatial_transformer' mode:
             frame_predictor_stn = STNFramePredictor(is_training=True, n_filters=16,
                                                     name='STNFramePredictor').build(tframe_hard_anatomy_t1,
                                                                                     tframe_input_times,
                                                                                     tframe_delta_times,
                                                                                     mode='spatial_transformer')
             # get predicted frame:
             tframe_pred = frame_predictor_stn.get_pred_frames()

             # add regularization loss on the transformer parameters:
             theta_values = frame_predictor_stn.get_theta_values()
             theta_loss = frame_predictor_stn.theta_regularization_loss(theta_values, batch_size=self.batch_size)

        """
        self.is_training = is_training
        self.nf = n_filters
        self.name = name
        self.theta_values = None

    def build(self, input_frames, input_times, delta_times, reuse=tf.AUTO_REUSE):
        """
        Build the model.
        """

        with tf.variable_scope(self.name, reuse=reuse):

            # - - - - - - -
            # build only UNet encoder for now: (n_out=n_w_masks+1 becouse the last one is for the identity matrix)
            unet = SmallUnet(incoming=input_frames, n_out=1, is_training=self.is_training)
            encoder = unet.build_encoder()

            # get latent code:
            latent_code = encoder[-2]  # (this is the output layer of the encoder)

            # - - - - - - -
            # concatenate encoded time information (output of MLP #1) :
            latent_shape = latent_code.get_shape().as_list()
            n_fraction = 8
            time_shape = [-1, latent_shape[1], latent_shape[2], (latent_shape[3] // n_fraction)]
            n_out = reduce(lambda x, y: x * y, latent_shape[1:]) // n_fraction

            times = tf.concat((tf.expand_dims(input_times, 1),
                               tf.expand_dims(delta_times, 1)), axis=1)
            mlp_in = MLP(times, 128, 128, n_out, self.is_training, k_prob=0.8, name='MLP_in').build()
            time_code = tf.reshape(mlp_in, shape=time_shape)
            latent_code_with_time = tf.concat((latent_code, time_code), axis=-1)

            # - - - - - - -
            # compute set of transformed input frames (output of MLP #2) :

            # extract the parameters and apply the transformation matrix to the input frames,
            # using the transformer module:
            self.transformed_frames, self.theta_values = MLPTransformer(mlp_in=latent_code_with_time,
                                                                        transformer_in=input_frames,
                                                                        mlp_units_in=128,
                                                                        mlp_units_hidden=128,
                                                                        mlp_units_out=6,
                                                                        is_training=self.is_training, k_prob=0.8,
                                                                        name='MLP_out').build()
            self.transformed_frames = self.transformed_frames[..., 0]

        return self

    def theta_regularization_loss(self, theta_values, batch_size):
        """
        Regularization loss for theta values: ideally should not be too far away from identity matrix (no big changes
        in time).
        :param theta_values: parameters of the transformation matrix to the input frames, computed in build()
        :param batch_size: (int) batch size (i.e. number of input frames)
        :return: regularization loss
        """
        with tf.variable_scope('ParameterEstimate_loss'):
            # parameters for identity matrices
            identity_params = np.array([[1., 0., 0.], [0., 1., 0.]], dtype=np.float32).flatten()
            identity_row = np.tile(identity_params, [batch_size])
            identity = np.reshape(identity_row, [batch_size, 6])
            identity = tf.constant(identity)

            params_loss = tf.reduce_mean(tf.squared_difference(theta_values, identity))

        return params_loss

    def get_transformed_frames(self):
        return self.transformed_frames

    def get_theta_values(self):
        return self.theta_values
