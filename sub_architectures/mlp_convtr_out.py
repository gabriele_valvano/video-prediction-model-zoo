import tensorflow as tf
from tensorflow import layers


# He initializer for the layers with ReLU activation function:
he_init = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False)
b_init = tf.zeros_initializer()


class MLPConvTransformer(object):

    def __init__(self, mlp_in, transformer_in, mlp_units_in, mlp_units_hidden, mlp_units_out, is_training,
                 kernel_size=5, k_prob=1.0, name='MLP'):
        """
        Class for 3-layered multilayer perceptron (MLP) + convolutional transformer module
        :param mlp_in: (tensor) incoming tensor for the mlp (i.e. latent features from which to extract the
                        transformation parameters)
        :param transformer_in: (tensor) incoming tensor for the transformer (i.e. images)
        :param mlp_units_in: (int) number of input units
        :param mlp_units_hidden: (int) number of hidden units
        :param mlp_units_out: (int) number of output units; it should be a multiple of kernel_size**2
                        (kernel_size**2 params for each transformer)
        :param is_training: (tf.placeholder(dtype=tf.bool) or bool) variable to define training or test mode; it is
                        needed for the behaviour of dropout (which behaves differently at train and test time)
        :param k_prob: (float) keep probability for dropout layer. Default = 1, i.e. no dropout applied. A common value
                        for keep probability is 0.8 (e.g. 80% of active units at training time)

        - - - - - - - - - - - - - - -

        Side notes:
          One good idea can be to initialize the transformer layer to the identity transform, initializing theta to :
             | identity = np.array([[1., 0., 0.], [0., 1., 0.]])
             | identity = identity.flatten()
             | theta_init = tf.Variable(initial_value=identity)
          Another option may be using a regularizer to be sure that theta is not too far away from the identity matrix.

        """

        assert 0.0 <= k_prob <= 1.0
        assert mlp_units_out % (kernel_size**2) == 0  # we need k_size^2 parameters for the each conv transformer

        self.mlp_in = mlp_in
        self.transformer_in = transformer_in
        self.n_in = mlp_units_in
        self.n_hidden = mlp_units_hidden
        self.k_size = kernel_size  # i.e. 5x5 filters
        self.n_out = mlp_units_out - (kernel_size**2)  # k_size^2 params per conv transformer (i.e. filters 5x5)
        self.is_training = is_training
        self.k_prob = k_prob
        self.name = name

    def build(self):
        """
        Build the model.
        """
        with tf.variable_scope(self.name):
            theta_values = self._mlp(self.mlp_in, name='mlp_0')
            transformed_input = self._conv_transformer(theta_values, name='transformers')
            return transformed_input

    def _mlp(self, incoming, name):
        """
        (3-layered) multilayer perceptron used to predict parameters theta for the transformer module.
        :param incoming: (tf.layer) incoming tensor
        :param name: (str) name_scope for the MLP
        :return: estimate for theta
        """
        # keep_prob = tf.cond(tf.equal(self.is_training, tf.constant(True)), lambda: self.k_prob, lambda: 1.0)

        with tf.variable_scope(name):
            incoming = layers.flatten(incoming)

            input_layer = layers.dense(incoming, self.n_in, kernel_initializer=he_init, bias_initializer=b_init)
            input_layer = tf.layers.batch_normalization(input_layer)
            input_layer = tf.nn.relu(input_layer)

            hidden_layer = layers.dense(input_layer, self.n_hidden, kernel_initializer=he_init, bias_initializer=b_init)
            hidden_layer = tf.layers.batch_normalization(hidden_layer)
            hidden_layer = tf.nn.relu(hidden_layer)

            output_layer = layers.dense(hidden_layer, self.n_out, bias_initializer=b_init)
            output_layer = tf.layers.batch_normalization(output_layer)

            # final activation: linear
        return output_layer

    def _conv_transformer(self, theta_values, name):
        """
        Convolutional transformer layers. Splits theta_values to build N filters and then convolve the input for each of
        these filters.
        :param theta_values: values for the transformation module
        :param name: (str) name_scope for the module
        :return: transformed input
        """

        with tf.variable_scope(name):

            # k_size^2 params for each transformer --> we need n_tr filters
            n_tr = self.n_out // (self.k_size**2)
            params_splits = tf.split(theta_values, n_tr, axis=1) if n_tr > 1 else [theta_values]
            tr_out = []

            # build a transformer for each split:
            for k in range(n_tr):
                # select and reshape filter to shape [filter_height, filter_width, in_channels, out_channels]:
                theta = params_splits[k]

                theta = tf.reduce_mean(theta, axis=0)  # TODO: fix to have a different kernel per batch

                # theta = tf.transpose(theta, [1,2,0,4,3])
                # theta = tf.reshape(theta, [self.k_size, self.k_size, -1, n_tr + 1])
                # self.transformer_in = tf.transpose(self.transformer_in[0], [1, 2, 3, 0])
                #
                # z = tf.nn.depthwise_conv2d(self.transformer_in, theta, [1, 1, 1, 1], 'SAME')
                # z = tf.reshape(z, [1, 120, 120, -1, 10])
                # z = tf.transpose(z, [3, 0, 1, 2, 4])

                # filter tensor of shape: [filter_height, filter_width, in_channels, channel_multiplier]
                theta = tf.reshape(theta, [self.k_size, self.k_size, 1, 1])  # -1 --> batch size  # TODO: check this

                # convolve such filter for each channel of the input
                with tf.variable_scope('conv_transformer_' + str(k)):
                    conv_stack = []
                    for s in range(n_tr + 1):
                        in_slice = tf.expand_dims(self.transformer_in[..., s], axis=-1)
                        conv = tf.nn.depthwise_conv2d(in_slice, theta, strides=[1, 1, 1, 1], padding='SAME')
                        conv_stack.append(conv)
                    conv_stack = tf.concat(conv_stack, axis=-1)

                    tr_out.append(conv_stack)

            # add also input images to the list (to allow an identity transform for static pixels)
            tr_out.append(self.transformer_in)

            tr_out = tf.stack(tr_out, axis=-1)

        return tr_out
