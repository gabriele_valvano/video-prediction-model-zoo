import tensorflow as tf
from tensorflow import layers
from layers.spatial_transformer_layer import transformer
import numpy as np


# He initializer for the layers with ReLU activation function:
he_init = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False)
b_init = tf.zeros_initializer()


class MLPTransformer(object):

    def __init__(self, mlp_in, transformer_in, mlp_units_in, mlp_units_hidden, mlp_units_out, is_training, k_prob=1.0,
                 name='MLP'):
        """
        Class for 3-layered multilayer perceptron (MLP) + transformer module
        :param mlp_in: (tensor) incoming tensor for the mlp (i.e. latent features from which to extract the
                        transformation parameters)
        :param transformer_in: (tensor) incoming tensor for the transformer (i.e. images)
        :param mlp_units_in: (int) number of input units
        :param mlp_units_hidden: (int) number of hidden units
        :param mlp_units_out: (int) number of output units; it should be a multiple of 6 (6 params for each transformer)
        :param is_training: (tf.placeholder(dtype=tf.bool) or bool) variable to define training or test mode; it is
                        needed for the behaviour of dropout (which behaves differently at train and test time)
        :param k_prob: (float) keep probability for dropout layer. Default = 1, i.e. no dropout applied. A common value
                        for keep probability is 0.8 (e.g. 80% of active units at training time)

        - - - - - - - - - - - - - - -

        Side notes:
          One good idea can be to initialize the transformer layer to the identity transform, initializing theta to :
             | identity = np.array([[1., 0., 0.], [0., 1., 0.]])
             | identity = identity.flatten()
             | theta_init = tf.Variable(initial_value=identity)
          Another option may be using a regularizer to be sure that theta is not too far away from the identity matrix.

        """

        assert 0.0 <= k_prob <= 1.0
        assert mlp_units_out % 6 == 0  # we need 6 parameters for the each transformer module

        self.mlp_in = mlp_in
        self.transformer_in = transformer_in
        self.n_in = mlp_units_in
        self.n_hidden = mlp_units_hidden
        self.n_out = mlp_units_out - 6  # 6 params per transformer: we have n_out//6 transformed images + input image
        self.is_training = is_training
        self.k_prob = k_prob
        self.name = name

    def build(self):
        """
        Build the model.
        """
        with tf.variable_scope(self.name):
            theta_values = self._mlp(self.mlp_in, name='mlp_0')
            transformed_input = self._transformer(theta_values, name='transformers')

            # last 6 parameters are equivalent to identity transformation parameters because the last channel is just
            # the input image:
            identity_params = np.array([[1., 0., 0.], [0., 1., 0.]], dtype=np.float32).flatten()
            theta_values_plus_identity = tf.map_fn(lambda x: tf.concat([x, identity_params], axis=-1), theta_values)

            return transformed_input, theta_values_plus_identity  # theta_values

    def _mlp(self, incoming, name):
        """
        (3-layered) multilayer perceptron used to predict parameters theta for the transformer module.
        :param incoming: (tf.layer) incoming tensor
        :param name: (str) name_scope for the MLP
        :return: estimate for theta
        """
        # keep_prob = tf.cond(tf.equal(self.is_training, tf.constant(True)), lambda: self.k_prob, lambda: 1.0)

        with tf.variable_scope(name):
            incoming = layers.flatten(incoming)

            input_layer = layers.dense(incoming, self.n_in, kernel_initializer=he_init, bias_initializer=b_init)
            input_layer = tf.layers.batch_normalization(input_layer)
            input_layer = tf.nn.relu(input_layer)

            hidden_layer = layers.dense(input_layer, self.n_hidden, kernel_initializer=he_init, bias_initializer=b_init)
            hidden_layer = tf.layers.batch_normalization(hidden_layer)
            hidden_layer = tf.nn.relu(hidden_layer)

            output_layer = layers.dense(hidden_layer, self.n_out, bias_initializer=b_init)
            output_layer = tf.layers.batch_normalization(output_layer)

            # final activation: linear
        return output_layer

    def _transformer(self, theta_values, name):
        """
        Transformer layers. Splits theta_values to build a number of N = self.n_out//6 transformers.
        :param theta_values: values for the transformation module
        :param name: (str) name_scope for the module
        :return: transformed input
        """

        with tf.variable_scope(name):
            out_shape = self.transformer_in.get_shape().as_list()[1:-1]

            # 6 params for each transformer --> we need n_tr transformers
            n_tr = self.n_out // 6
            params_splits = tf.split(theta_values, n_tr, axis=1) if n_tr > 1 else [theta_values]

            tr_out = []

            # build a transformer for each split:
            for k in range(n_tr):
                theta = params_splits[k]
                with tf.variable_scope('transformer_' + str(k)):
                    tr_out.append(transformer(U=self.transformer_in, theta=theta, out_size=out_shape))

            # add also input images to the list (to allow an identity transform for static pixels)
            tr_out.append(self.transformer_in)

            tr_out = tf.stack(tr_out, axis=-1)

        return tr_out
